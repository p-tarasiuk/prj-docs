# Project Management

This group of documents defines the goals and scope of the project, its functional and non-functional requirements, and the specific functionality to be implemented.
They help the project manager and the development team navigate current tasks and priorities.