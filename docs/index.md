# Project Documentation (Example)

Welcome to an example of documenting a website development project.
Here you will find all the information you need about project development and management.

Project documentation consists of the following sections:

- [ ] [Business Planning](bp/index.md)
    [=85%]{: .thin}
- [ ] [Project Management](pm/index.md)
    [=60%]{: .thin}
- [ ] [Architecture Design](ad/index.md)
    [=45%]{: .thin}
- [ ] [Brand Book](bb/index.md)
    [=30%]{: .thin}
- [ ] [Content Making](cm/index.md)
    [=0%]{: .thin}
- [ ] [Quality Assurance](qa/index.md)
    [=0%]{: .thin}

!!! note

    The performance bar shows how much percentage is filled with a particular section

!!! tip

    Feel free to navigate through the documentation using the links above.
