# Brand Book

This group of documents describes all materials and resources related to the brand identity and design of the project.
It includes files: Design Brief, Style Guidelines, logos, graphic elements, design templates, photos and other important materials that determine the visual style and aesthetics of the project.
This group of documents is a centralized point of access to all the necessary resources to develop the appropriate brand identity and design, and ensures consistency and visual unity in all materials related to the project.

- [x] [Brand Name](brand-name.md)
- [ ] [Design Brief](brief.md)
- [ ] [Style Guidelines](style.md)