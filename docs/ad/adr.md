# Architecture Design Record (ADR)

## ADR 1: Deploying WordPress in Docker Containers on GCP Cloud Run with Cloud SQL

### 1. Status

Accepted

### 2. Context

The "Glamour Avenue" Online Boutique project requires a reliable, scalable, and easily manageable platform for deploying its website on WordPress.
Various hosting and deployment options were considered, including traditional web hosting, self-managed virtual servers, and containerization.

### 3. Decision

We decided to deploy WordPress in Docker containers on Google Cloud Platform (GCP) using Cloud Run for dynamic container management and Cloud SQL for MySQL database.
This decision was made considering the following advantages:

- **Scalability**: Cloud Run automatically scales containers based on load, ensuring efficient resource utilization.
- **Management**: Docker simplifies managing dependencies and environments, ensuring consistency between development, testing, and production.
- **Reliability**: Cloud SQL provides a highly available and reliable database with backup and restore capabilities.
- **Security**: GCP offers advanced security features, including network firewalls, data encryption, and access management.

### 4. Alternatives Considered

- **Traditional Web Hosting**: Lack of flexibility and control over the environment.
- **Self-Managed Virtual Servers** (e.g., EC2 on AWS): Higher infrastructure management requirements.
- **Kubernetes**: Possibly too complex for the project's needs.

### 5. Consequences

- **Development and Deployment**: Developers need to have knowledge of Docker and CI/CD processes to work effectively with this architecture.
- **Costs**: GCP costs should be closely monitored as scaling services may lead to increased expenses.
- **Vendor Lock-in**: The project becomes dependent on GCP services and pricing.

## ADR 2: Choosing Cloud SQL for WordPress Database

### 1. Status

Accepted

### 2. Context

The project requires a reliable database management system for WordPress. Various options were considered, including using the built-in MySQL database on shared hosting, self-deployed MySQL on a virtual machine, and using a managed database as a service.

### 3. Decision

We choose Cloud SQL (MySQL) from GCP as the managed database service for WordPress, considering its reliability, ease of management, and integration with Cloud Run.

### 4. Consequences

- **Improved Reliability**: Cloud SQL provides a highly available and reliable database infrastructure with automated backups and recovery options.
- **Ease of Management**: Cloud SQL simplifies database administration tasks such as scaling, replication, and monitoring, reducing operational overhead.
- **Integration with Cloud Run**: Seamless integration between Cloud SQL and Cloud Run enables efficient data access and management for the WordPress application.
- **Additional Costs**: While Cloud SQL offers numerous benefits, there are additional costs associated with using a managed service, which should be factored into the project budget.

## ADR 3: Using GitLab for CI/CD and Container Registry

### 1. Status

Accepted

### 2. Context

The project requires a reliable system for automating deployment and version control. Various CI/CD tools were considered, including Jenkins, GitHub Actions, and GitLab CI/CD.

### 3. Decision

We choose GitLab for code repository, CI/CD processes, and container registry due to its integration, ease of use, and container registry functionality.

### 4. Consequences

- **Streamlined Development Workflow**: GitLab's integrated CI/CD pipelines enable automated testing, building, and deployment of application code, leading to a more efficient development process.
- **Container Registry Functionality**: GitLab Container Registry provides a secure and private repository for storing Docker images, facilitating versioning and distribution of application containers.
- **Centralized Collaboration**: GitLab's collaborative features such as merge requests, issue tracking, and code review enhance team collaboration and productivity.
- **Setup and Maintenance Overhead**: While GitLab offers comprehensive features, setting up and maintaining CI/CD pipelines and the container registry may require initial investment in time and resources.

## ADR 4: Deployment on Cloud Run for Scalability and Management

### 1. Status

Accepted

### 2. Context

It's necessary to decide where and how to deploy WordPress Docker containers, considering scalability and management needs.

### 3. Decision

We decide to utilize Cloud Run from GCP for container deployment, as it provides easy scalability, high availability, and convenient management without the need to manage server infrastructure.

### 4. Consequences

- **Elasticity**: Cloud Run automatically scales containers based on incoming traffic, ensuring optimal resource utilization and cost efficiency.
- **High Availability**: Cloud Run offers built-in redundancy and fault tolerance, minimizing downtime and ensuring reliability for the WordPress application.
- **Serverless Management**: Cloud Run abstracts away server management tasks, allowing developers to focus on application development rather than infrastructure maintenance.
- **Vendor Lock-in**: While Cloud Run offers numerous benefits, there is a risk of vendor lock-in, as the application becomes tightly coupled with Google Cloud Platform services. It's essential to evaluate the long-term implications and consider mitigating strategies such as adopting containerization standards like Kubernetes.

---

These ADRs help document and justify key decisions affecting the architecture and design of the "Glamour Avenue" Online Boutique, providing transparency and ease of implementing changes in the future.