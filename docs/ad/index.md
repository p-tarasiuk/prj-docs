# Architecture Design

This group of documents describes the overall design of the project's architecture, its components, technological solutions, and basic implementation principles.
They are necessary for understanding the technical aspects of the project and making key architectural decisions.
