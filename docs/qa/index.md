# Quality Assurance

This group of documents describes strategies and test plans for software quality assurance.
They help ensure that the site has high functional quality and meets the set requirements.

- **Test Plan**: Describes the strategy and test plan for testing functionality, performance, security, and other aspects of the site.
- **Test Results**: Captures test results, detected errors and planned fixes.
- **Quality Assurance Plans**: Include procedures and practices designed to ensure product quality during and after development.
