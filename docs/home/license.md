# License

The legal stuff.

---

## Included projects

- MkDocs - [View License](https://github.com/mkdocs/mkdocs/blob/master/LICENSE)
- Material for MkDocs - [View License](https://github.com/squidfunk/mkdocs-material/blob/master/LICENSE)
- PDF Generate Plugin for MkDocs - [View License](https://github.com/orzih/mkdocs-with-pdf/blob/master/LICENSE)
- Markdown-Include - [View License](https://github.com/cmacmackin/markdown-include/blob/master/LICENSE.txt)
- MkDocs static i18n plugin - [View License](https://github.com/ultrabug/mkdocs-static-i18n/blob/main/LICENSE)
- PyMdown Extensions - [View License](https://github.com/facelessuser/pymdown-extensions/blob/main/LICENSE.md)

Many thanks to the authors and contributors of those wonderful projects.

{!../LICENSE!}
