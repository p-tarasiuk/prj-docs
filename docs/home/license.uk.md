# Ліцензія

Юридичні речі.

---

## Включені проекти

- MkDocs - [Переглянути ліцензію](https://github.com/mkdocs/mkdocs/blob/master/LICENSE)
- Material for MkDocs - [Переглянути ліцензію](https://github.com/squidfunk/mkdocs-material/blob/master/LICENSE)
- PDF Generate Plugin for MkDocs - [Переглянути ліцензію](https://github.com/orzih/mkdocs-with-pdf/blob/master/LICENSE)
- Markdown-Include - [Переглянути ліцензію](https://github.com/cmacmackin/markdown-include/blob/master/LICENSE.txt)
- MkDocs static i18n plugin - [Переглянути ліцензію](https://github.com/ultrabug/mkdocs-static-i18n/blob/main/LICENSE)
- PyMdown Extensions - [Переглянути ліцензію](https://github.com/facelessuser/pymdown-extensions/blob/main/LICENSE.md)

Велике спасибі авторам і учасникам цих чудових проектів.

{!../LICENSE!}
