# URLs

- [MkDocs](https://www.mkdocs.org/)
- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- [Open Water Foundation / Learn MkDocs](https://learn.openwaterfoundation.org/owf-learn-mkdocs/)
- [PyMdown Extensions](https://facelessuser.github.io/pymdown-extensions/)
- [MkDocs site localization made easy](https://ultrabug.github.io/mkdocs-static-i18n/)